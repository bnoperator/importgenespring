# Import Genespring file
 
Import data using a Genespring file format.
 
# Create app and deploy

Clone the repo and build the package then from the package folder run the following.

```
bntools::createApp(tags=c('Import data', 'Test'), mainCategory = 'Import data')

```

```
git add -A && git commit -m "add tag Test" && git push && git tag -a 1.4 -m "++" && git push --tags
```

```
bntools::deployGitApp('https://bitbucket.org/bnoperator/importgenespring.git', '1.4')
```
  

```
bnshiny::startBNTestShiny('importgenespring')
# see workspace.R for a full example
```

# Change
## 1.0
- first version
## 1.1
- add missing imports
## 1.2
- add correct version in padf file
## 1.3
- add tag Test
## 1.4
- add correct version in padf file
