library(importgenespring)

context("genespring import format")

test_that("convert.gs.df.to.cube", {

  df = data.frame(c1=c('Samples ()', 'Conc ()', 'gene1', 'gene2', 'gene3'),
                  c2=c('sample1', '0.1', '1.0', '10.0', '100.0'),
                  c3=c('sample2', '0.2', '2.0', '20.0', '200.0'))

  cube = convert.gs.df.to.cube(df)

  expect_that(length(cube$dataNames), equals(1))
  expect_that(cube$dataNames, equals(c('value')))

  expect_that(dim(cube$colData$data), equals(c(2,2)))
  expect_that(dim(cube$rowData$data), equals(c(3,1)))
})
